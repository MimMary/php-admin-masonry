<?php 
  require_once 'connection.php';
   $result = $conn->query('SELECT * FROM images');
   $rows = $result->fetch_all(MYSQLI_ASSOC);
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="stylesheet" href="css/style.css" />
    <title>Document</title>
  </head>
  <body>
    <ul class="masonry">

      <?php  foreach($rows as $value){ ?>
        <li class="masonry__item">
          <img class="masonry__image" src="<?= "images/".$value["path"]?>" alt="car" style = "border: 2px solid <?php echo $value["color"]?>">
        </li>
      <?php } ?>

    </ul>
    <script src="./javascript/script.js"></script>
  </body>
</html>
