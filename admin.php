<?php
 require_once 'connection.php';

 $color = $_POST['color'];
 $file = $_FILES['file'];
 $title = $_POST['title'];
 $submit = $_POST['submit'];
 $save = $_POST['save'];
 $remove = $_POST['remove'];
 $created_at = date('Y-m-d H:i:s');

 if($color && $_FILES['file']['size'] > 0 && $title) {
   
  $uploaddir = './images/';
  $fileName = basename($_FILES['file']['name']);
  $uploadfile = $uploaddir . $fileName;
  
  if (move_uploaded_file($_FILES['file']['tmp_name'], $uploadfile)) {
      echo "Successfully uploaded.\n";
  } else {
      echo "Fail!\n";
  }

  $sql = "INSERT INTO images(color, path, title, created_at)  VALUES ('$color', '$fileName', '$title', '$created_at')";
  mysqli_query($conn , $sql);

 } 

 else if (isset($_POST['save'])) {
  $id = $_POST['id'];
  $record = mysqli_query($conn, "UPDATE `images` SET title='$title', color='$color' WHERE id={$id}");
 }

 else if (isset($_POST['remove'])) {
  $id = $_POST['id'];
  $record = mysqli_query($conn, "DELETE FROM `images` WHERE id={$id}");
 }

 $result = $conn->query('SELECT * FROM images');
 $rows = $result->fetch_all(MYSQLI_ASSOC);

?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="/css/style.css" >
  <title>Document</title>
</head>
<body>
  <div class="page_form">
    <form class="form" action="admin.php" method="post" enctype="multipart/form-data">
      <input  type="file" name="file">
      <input class="form_inp" type="title" name="title" placeholder="title">
      <input class="form_inp" type="text" name="color" placeholder="color">
      <input class="form_btn" type="submit" name="submit" value="add">
    </form>

    <?php  foreach($rows as $value){ ?>
      
      <form class="form" action="admin.php" method="post">
        <img class="img" src="<?= "images/".$value["path"]?>" style = "border: 2px solid <?php echo $value["color"]?>">
        <input class="form_inp" type="title" name="title" placeholder="title" value = <?php echo $value["title"]?> >
        <input class="form_inp" type="text" name="color" placeholder="color" value="<?php echo $value["color"]?>">
        <input class="form_btn" type="submit" name="save" value="save">
        <input type="hidden" name="id" value="<?= $value['id']?>">
        <input class="form_btn_remove" type="submit" name="remove" value="remove">
      </form>
      
    <?php } ?>

  </div>
</body>
</html>